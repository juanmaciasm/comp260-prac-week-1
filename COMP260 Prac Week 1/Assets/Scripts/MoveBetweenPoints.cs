﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenPoints : MonoBehaviour {
    //public parameters
    public Vector3 startPoint;
    public Vector3 endPoint;
    public float speed = 1.0f;

    //private state
    private bool movingForward = true;


	// Use this for initialization
	void Start () {
        transform.position = startPoint;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 target;

        if (movingForward)
        {
            target = endPoint;
        } else
        {
            target = startPoint;
        }


        float distanceToMove = speed * Time.deltaTime;

        float distanceToTarget = (target - transform.position).magnitude;
        Debug.Log("distance to target = " + distanceToTarget);

        if(distanceToMove > distanceToTarget)
        {
            transform.position = target;
            movingForward = !movingForward;
            Debug.Break(); //pause

        } else
        {
            Vector3 dir = (target - transform.position).normalized;
            transform.position += dir * distanceToMove;
        }
	}
}
